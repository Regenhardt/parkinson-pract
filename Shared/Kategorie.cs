﻿namespace ParkinsonPract.Shared;

public record Kategorie(string Route, string Anzeigename, string Icon, string Hintergrundfarbe)
{
    public static readonly Kategorie Koerperaktivierung = new("/kategorie/koerperaktivierung", "Körperaktivierung", "oi oi-pulse", "#fad9d5");
    public static readonly Kategorie Stimmaktivierung = new("/kategorie/stimmaktivierung", "Stimmaktivierung", "oi oi-microphone", "#cdeb8b");
    public static readonly Kategorie Atmung = new("/kategorie/atmung", "Atmung", "oi oi-heart", "#dae8fc");
    public static readonly Kategorie MimikGesicht = new("/kategorie/mimik-gesicht", "Mimik/<wbr />Gesichtsmuskulatur", "oi oi-face-grin", "#fff2cc");
    public static readonly Kategorie Artikulation = new("/kategorie/artikulation", "Artikulation", "oi oi-speech-bubble", "#e1d5e7");
    public static readonly Kategorie Stimmkraft = new("/kategorie/stimmkraft", "Stimmkraft", "oi oi-volume-high", "#bac8d3");
    public static readonly Kategorie Lautstaerke = new("/kategorie/lautstaerke", "Lautstärke", "oi oi-volume-low", "#f5f5f5");
    public static readonly Kategorie Schlucken = new("/kategorie/schlucken", "Schlucken", "oi oi-fork", "#ffcc99");

    public static readonly List<Kategorie> Kategorien = new()
    {
        Koerperaktivierung,
        Stimmaktivierung,
        Atmung,
        MimikGesicht,
        Artikulation,
        Stimmkraft,
        Lautstaerke,
        Schlucken
    };
}
