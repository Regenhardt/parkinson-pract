﻿namespace ParkinsonPract.Shared;

public class Uebung
{
    public string Titel { get; set; }

    public string Beschreibung { get; set; }

    public Kategorie Kategorie { get; set; }

    public string Video { get; set; }
}
