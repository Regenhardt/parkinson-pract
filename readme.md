# ParkinsonPract

## Use Cases

### Use Case Diagramm

![Diagram](./useCase.drawio.svg)

### Szenarios

UC1: Video ansehen
- Akteur: Patient
- Ziel: Patient möchte ein Video ansehen
- Vorbedingungen: 
  - Patient ist angemeldet
  - Internet ist vorhanden
  - Patient befindet sich auf der Startseite
  - Kategorien werden angezeigt
- Nachbedingungen: 
  - Video wird angezeigt
- Erfolgsszenario:
    1. Patient wählt Kategorie aus
    2. System zeigt Übungen aus der Kategorie an
    3. Patient wählt Video aus
    4. System zeigt Video an und spielt es automatisch ab
- Erweiterungsfall 5a: Bestätigung
    1. Video ist beendet
    2. Patient bestätigt durchführung der Übung
    3. Patient wird zur Übersicht der Übungen in dieser Kategorie geleitet

UC2: Anmelden
- Akteur: Patient/Logo
- Ziel: Nutzer möchte sich anmelden
- Vorbedingungen: 
  - Nutzer befindet sich auf der Startseite
  - Nutzer ist nicht angemeldet
  - Nutzer hat ein gültiges Konto
- Nachbedingungen:
    - Nutzer ist angemeldet
    - Nutzer ist auf der Startseite
- Erfolgsszenario:
    1. Nutzer gibt seinen Identifikator ein (z.B. E-Mailadresse)
    2. Nutzer bestätigt seine Eingabe
    3. System überprüft Identifikator
    4. System zeigt Passwortfeld an
    5. Nutzer gibt Passwort ein
    6. Nutzer bestätigt seine Eingabe
    7. System überprüft das Passwort
    8. System leitet Nutzer zur Startseite weiter
    9. System zeigt Kategorien an
- Erweiterungsfall 4a: Nutzer hat biometrischen Login aktiviert
    1. System zeigt Biometrischen Login an
    2. Nutzer bestätigt Login
    3. System überprüft biometrischen Login
    4. Weiter mit 8
- Erweiterungsfall 7a: Passwort falsch
    1. System zeigt Hinweismeldung an
    2. Nutzer gibt korrektes Passwort ein
    3. Weiter mit 6

UC4: Nachricht lesen (Patient)
- Akteur: Patient
- Ziel: Patient möchte eine Nachricht des Logos lesen
- Vorbedingungen: 
  - Patient ist angemeldet
  - Patient befindet sich auf der Startseite
- Nachbedingungen: 
  - Nachricht wird angezeigt
- Erfolgsszenario:
    1. Patient wählt den Postfach-Button aus
    2. System zeigt Nachrichten des Logos an
- Erweiterungsfall 2a: Logo hat den Patienten noch nicht bei sich registriert
    1. System zeigt Hinweismeldung und einen OK-Button an, der zur Startseite führt

UC10: Nachricht lesen  (Logo)
- Akteur: Logo  
- Ziel: Logo möchte eine Nachricht eines Patienten lesen  
- Vorbedingungen:    
  - Logo ist angemeldet  
  - Logo befindet sich auf der Startseite  
  - Logo hat eine Nachricht von einem Patienten    
- Nachbedingungen:  
  - System zeigt die Nachricht an
  - Logo kann die Nachricht lesen
  - Nachricht ist als gelesen markiert  
- Erfolgsszenario:  
    1. Logo wählt Postfach-Button aus
    2. System zeigt Konversationen an
    3. Logo wählt Konversation aus
    4. System zeigt alle Nachrichten dieser Konversation an

UC12: Patient hinzufügen
- Akteur: Logo
- Ziel: Logo möchte einen Patienten hinzufügen
- Vorbedingungen: 
  - Logo ist angemeldet
  - Logo befindet sich auf der Startseite
  - Logo hat erfahren, dass sein Patient eine Verordnung für ParkinsonPract hat
  - Patient hat sich registriert
- Nachbedingungen:
    - Logo ist mit Patient verknüpft
- Erfolgsszenario:
    1. Logo stellt sich selbst einen Patientenidentifikator zusammen (z.B. 1. Buchstabe des Vornamen, letzter Buchstabe des Nachnamen, Geburtsjahr)
    2. Logo wählt Patient hinzufügen aus
    3. System zeigt Eingabefeld an
    4. Logo gibt Patientenidentifikator ein
    5. Logo bestätigt seine Eingabe
    6. System überprüft Identifikator
    7. System zeigt passende Patienten an
    8. Logo wählt Patienten aus
    9. System verknüpft Logo mit Patienten
- Erweiterungsfall 7a: Identifikator ist ungültig
    1. System zeigt Hinweismeldung an
    2. Logo gibt korrekten Identifikator ein
    3. Weiter mit 6

## Arbeitspakete

Arbeitspakete sind als Issies in Gitlab einsehbar.